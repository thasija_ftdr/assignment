package main

import (
	"bytes"
	"fmt"
	"frontdoorassignment/cool"
	"io/ioutil"
	"log"
	"net/http"

	b64 "encoding/base64"

	"google.golang.org/protobuf/proto"
)

// Making Post Request
func makePostRequest(request *cool.UserRequest) string {
	// Marshalling
	req, err := proto.Marshal(request)
	if err != nil {
		log.Fatalf("Unable to marshal request : %v", err)
	}
	// connecting using HTTP post
	resp, err := http.Post("http://0.0.0.0:8080/assignment/user", "application/x-binary", bytes.NewReader(req))
	if err != nil {
		log.Fatalf("Unable to read from the server : %v", err)
	}
	respBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Fatalf("Unable to read bytes from request : %v", err)
	}
	//returning the Response ObjectID
	return string(respBytes)

}

func makeGetRequest(request *cool.Request) *cool.UserResponse {
	// Marshalling request data
	req, err := proto.Marshal(request)
	if err != nil {
		log.Fatalf("Unable to marshal request : %v", err)
	}
	// Encoding The Data
	sEnc := b64.StdEncoding.EncodeToString([]byte(req))

	// Passing the data using query Parameters
	resp, err := http.Get("http://0.0.0.0:8080/assignment/user?proto_body=" + sEnc)

	if err != nil {
		log.Fatalf("Unable to read bytes from request : %v", err)
	}
	// Reading The Response Body
	respBytes, _ := ioutil.ReadAll(resp.Body)
	responseReceived := &cool.UserResponse{}
	// Unmarshal the response body
	proto.Unmarshal(respBytes, responseReceived)
	// Returning Result
	return responseReceived
}

func makePatchRequest(request *cool.Request) {
	// Marshalling Request
	data, err := proto.Marshal(request)

	if err != nil {
		log.Fatal(err)

	}
	// Encoding Request
	sEnc := b64.StdEncoding.EncodeToString([]byte(data))

	// Calling Patch Request
	req, err := http.NewRequest("PATCH", "http://0.0.0.0:8080/assignment/user?proto_body="+sEnc, bytes.NewReader([]byte("")))
	req.Header.Set("Content-Type", "application/octet-stream")

	if err != nil {
		log.Fatalf("Unable to read from the server : %v", err)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Id after Patch request:", string(body))
}

func main() {
	// Making Data for Post Request
	request := &cool.UserRequest{Firstname: "Karun",
		Lastname:    "Ahuja",
		Email:       "kahuja@gmail.com",
		Designation: "Software Engineer"}
	// Making Post Request
	resp := makePostRequest(request)
	// Printing ObjectID
	fmt.Println("Id of Data Sent = ", resp)

	// Get Request
	get := &cool.Request{
		Id: "5f745f976a59679701d538c4",
	}
	// Making get request
	responseGet := makeGetRequest(get)
	// Printing response
	fmt.Println("Data Receive from Database = ", responseGet.GetFirstname(), responseGet.GetLastname(), responseGet.GetEmail(), responseGet.GetDesignation())

	// Data for patch request
	id := "5f745f976a59679701d538c4"
	email := "ram_ali@gmail.com"

	// Creating Request
	requestPatch := &cool.Request{
		Id:    id,
		Email: email,
	}

	// Making Patch Request
	makePatchRequest(requestPatch)

}
