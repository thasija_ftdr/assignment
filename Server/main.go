package main

// imports
import (
	"context"
	"fmt"
	"frontdoorassignment/cool"
	"frontdoorassignment/db"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	b64 "encoding/base64"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/protobuf/proto"
)

// Collection Declaration
var UserCollection *mongo.Collection
var EmployeeCollection *mongo.Collection

// Get Call
func Getting(resp http.ResponseWriter, req *http.Request) {
	// Query Parameter
	keys, ok := req.URL.Query()["proto_body"]
	if !ok || len(keys) < 1 {
		log.Fatal("Error in key")
	}
	key := keys[0]

	// Decoding the 64BitEncoding
	sDec, _ := b64.StdEncoding.DecodeString(key)
	request := &cool.Request{}
	// Unmarshalling
	proto.Unmarshal(sDec, request)
	// Getting the ObjectID
	docID, _ := primitive.ObjectIDFromHex(request.GetId())
	response := &cool.UserResponse{}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	// Finding The Data From UserCollection
	err := UserCollection.FindOne(ctx, bson.M{"_id": docID}).Decode(&response)
	if err != nil {
		log.Println(err)
	}
	// Finding the Designation from EmployeeCollection
	err1 := EmployeeCollection.FindOne(ctx, bson.M{"userId": docID}).Decode(&response)
	if err1 != nil {
		log.Println(err1)
	}
	// Marshalling The Result
	result, errr := proto.Marshal(response)
	if errr != nil {
		log.Fatal(errr)
	}
	// returning Result
	resp.Write(result)
}

func Posting(resp http.ResponseWriter, req *http.Request) {
	data, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Fatal("Error While Reading")
	}
	request := &cool.UserRequest{}
	// UnMarshalling The Request
	proto.Unmarshal(data, request)
	fmt.Println(request)

	// MongoDB CODE AFTER THIS
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	// Inserting Into UserDatabase
	result, err := UserCollection.InsertOne(ctx, bson.D{
		{"firstname", request.GetFirstname()},
		{"lastname", request.GetLastname()},
		{"email", request.GetEmail()},
	})
	if err != nil {
		log.Fatal(err, "Error while Inserting Data")
	}
	id := result.InsertedID
	// Inserting Into Employee Database
	_, err1 := EmployeeCollection.InsertOne(ctx, bson.D{
		{"userId", id},
		{"designation", request.GetDesignation()},
	})
	if err1 != nil {
		log.Fatal("Error while inserting employee")
	}
	x := id.(primitive.ObjectID).String()
	//  Returning The Result
	resp.Write([]byte(x))

}

func Patching(resp http.ResponseWriter, req *http.Request) {

	// Getting The Value from the Query Parameters
	keys, ok := req.URL.Query()["proto_body"]
	if !ok || len(keys) < 1 {
		log.Fatal("Error in key")
	}
	key := keys[0]

	// Decoding the 64BitEncoding
	sDec, _ := b64.StdEncoding.DecodeString(key)
	request := &cool.Request{}
	// Unmarshal the request
	err1 := proto.Unmarshal(sDec, request)
	if err1 != nil {
		log.Fatal(err1)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	id, _ := primitive.ObjectIDFromHex(request.GetId())
	// Updating the Database
	result, err := UserCollection.UpdateOne(
		ctx,
		bson.M{"_id": id},
		bson.D{
			{"$set", bson.D{{"email", request.GetEmail()}}},
		},
	)
	if err != nil {
		log.Fatal(err)
	}
	// Printing the ModifiedCount
	fmt.Println(result.ModifiedCount)

	resp.Write([]byte(request.GetId()))
}

func main() {
	// API Server
	fmt.Println("Starting the API server...")
	// MUX handlers
	r := mux.NewRouter()
	r.HandleFunc("/assignment/user", Getting).Methods("Get")
	r.HandleFunc("/assignment/user", Posting).Methods("Post")
	r.HandleFunc("/assignment/user", Patching).Methods("Patch")

	// server Config
	server := &http.Server{
		Handler:      r,
		Addr:         "0.0.0.0:8080",
		WriteTimeout: 2 * time.Second,
		ReadTimeout:  2 * time.Second,
	}
	// Connecting to database
	database := db.GetClient()
	UserCollection = database.Collection("UserCollection")
	EmployeeCollection = database.Collection("EmployeeCollection")

	log.Fatal(server.ListenAndServe())

}
