module frontdoorassignment

go 1.15

require (
	github.com/golang/protobuf v1.4.2
	github.com/gorilla/mux v1.8.0
	go.mongodb.org/mongo-driver v1.4.1
	google.golang.org/protobuf v1.25.0
)
