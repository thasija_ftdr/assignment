# Assignment

Assignment for Backend using mongoDB gorilla-MUX and protocol-buffer


**Client**

    **main.go -** 

    this file contain calls to Gorilla-MUX API 
    Get - Use to retrieve database object using UserCollection Id
    Post - Insert data in database by sending data and returns Id
    Patch - update data in database and returns Id


**Server**
 
    **main.go-**
    
    this file contain Gorilla-Mux server handlers to handle Get Post Patch Request




**cool**
 
    contain proto file and compiled go protofile 
    wow.proto
    wow.pb.go


**db**
    
    **mongo.go -**
    
    connects to MongoDB cluster with MongoDB go driver using Mongo Atlas URI 
    and return database client 
